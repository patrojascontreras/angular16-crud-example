# Angular16-CRUD-Example

## Descripción
Proyecto de desarrollo web realizado en Angular versión 16.1.0 que el cuál consiste en un CRUD de datos vinculados a personas.

### Estructura del proyecto:

![Estructura del proyecto](/images/angular16_estructure-project-crud-example.jpg)

### Pasos ejecución y construcción del proyecto: 

**Paso 01:** Creación del proyecto: 

![Paso 01 - 0001](/images/Paso01_Angular16.jpg) 

![Paso 01 - 0002](/images/Paso02_Angular16.jpg) 

![Paso 01 - 0003](/images/Paso03_Angular16-001.jpg) 

![Paso 01 - 0004](/images/Paso03_Angular16-002.jpg) 

![Paso 01 - 0005](/images/Paso03_Angular16-003.jpg) 

**Paso 02:** Definición y configuración de librerías a utilizar: 
1. Agregar mediante comandos las siguientes librerías: 

* **npm install bootstrap@5.2.3 --legacy-peer-deps**
* **npm install @ng-bootstrap/ng-bootstrap@14.0.0 --legacy-peer-deps**
* **npm install sweetalert2 --legacy-peer-deps**
* **npm install jquery @popperjs/core --legacy-peer-deps**

2. Configuración de funcionamiento de Bootstrap: 

![Configuración de Bootstrap](/images/angular16_bootstrap-configuration.jpg) 

**Paso 03:** Creación de archivos determinados vinculados al proyecto mediante los siguientes comandos: 

* **ng generate component components/persons**
* **ng generate class models/person --type=model**
* **ng generate class models/personEntity --type=model**
* **ng generate class models/category --type=model** 
* **ng generate service services/person** 
* **ng generate service services/category** 

**Paso 04:** Proceso de despliegue del proyecto: 

![Paso 04 - 0001](/images/Paso04_Angular16-001.jpg) 

![Paso 04 - 0002](/images/Paso04_Angular16-002.jpg) 

**Paso 05:** Ejecución de la aplicación vinculada al proyecto: 

Ingresar al navegador mediante la url http://localhost:4200/

![Paso 05 - 0001](/images/deployment_angular16-0001.jpg) 

![Paso 05 - 0002](/images/deployment_angular16-0002.jpg) 

![Paso 05 - 0003](/images/deployment_angular16-0003.jpg) 

![Paso 05 - 0004](/images/deployment_angular16-0004.jpg) 

![Paso 05 - 0005](/images/deployment_angular16-0005.jpg) 

![Paso 05 - 0006](/images/deployment_angular16-0006.jpg) 

![Paso 05 - 0007](/images/deployment_angular16-0007.jpg) 

![Paso 05 - 0008](/images/deployment_angular16-0008.jpg) 

![Paso 05 - 0009](/images/deployment_angular16-0009.jpg) 

### Avisos importantes: 

* La realización de este proyecto consiste en la migración a Angular 16 de los proyectos CRUD-Example que se encuentran en los siguientes repositorios: https://gitlab.com/patrojascontreras/fullstack_patriciorojas_2020 y https://gitlab.com/patrojascontreras/fullstack_patriciorojas_2020_version-2023 
* En caso de que el proyecto se encuentre creado y se haya descargado a otro pc, solamente se debe ejecutar el comando **npm install** para el funcionamiento de las mismas dependencias. 
* Originalmente se aprendió este tipo de versiones de Angular desde que usé Angular 10 a finales del año 2020, pero por motivos de tiempo se pudo realizar en el día de hoy este proyecto. 
* Para la ejecución de test unitarios, se debe usar el siguiente comando: **ng test** 
* Para la ejecución de Build, se debe usar el siguiente comando: **ng build** 
* Con respecto a la ejecución de end-to-end tests, se debe utilizar el siguiente comando: **ng e2e** 
* Para temas de ayuda en Angular CLI dentro de un proyecto, se debe utilizar el siguiente comando: **ng help** 

Realizado por Ing. Patricio Rojas Contreras - 04 de julio de 2024. 

