import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Person } from '../models/person.model';
import { PersonEntity } from '../models/person-entity.model';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class PersonService {

  private urlReadPersonsListAll = environment.urlReadPersonsListAll;
  private urlReadPersonEdit = environment.urlReadPersonEdit;
  private urlReadPersonDetails = environment.urlReadPersonDetail;
  private urlPersonCreate = environment.urlPersonCreate;
  private urlPersonUpdate = environment.urlPersonUpdate;
  private urlPersonDelete = environment.urlPersonDelete;
  private urlCheckEmailExist = environment.urlCheckEmailExist;

  constructor(private http: HttpClient) { }

  getPersonsListAll(): Observable<PersonEntity[]> {
    return this.http.get<PersonEntity[]>(this.urlReadPersonsListAll);
  }

  getPersonsListAllById(idPerson: number): Observable<PersonEntity[]> {
    return this.http.get<PersonEntity[]>(this.urlReadPersonsListAll + '/' + idPerson);
  }

  personDetails(idPerson: number): Observable<PersonEntity> {
		return this.http.get<PersonEntity>(this.urlReadPersonDetails + '/' + idPerson);
	}

  personEdit(idPerson: number): Observable<Person> {
    return this.http.get<Person>(this.urlReadPersonEdit + '/' + idPerson);
  }

  personCreate(data: any): Observable<any> {
    return this.http.post(this.urlPersonCreate, data);
  }

  personUpdate(data: any): Observable<any> {
    return this.http.put(this.urlPersonUpdate, data);
  }

  personDelete(idNumber: number): Observable<any> {
    return this.http.delete(this.urlPersonDelete + '/' + idNumber);
  }

  checkEmailExist(email: string): Observable<Person> {
    return this.http.get<Person>(this.urlCheckEmailExist + '/' + email);
  }

}
