// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false, 
    urlReadPersonsListAll: 'http://localhost:8080/SpringBoot-CRUD-Example/api/persons/read/innerjoin/list', 
    urlReadCategoriesListAll: 'http://localhost:8080/SpringBoot-CRUD-Example/api/categories/read/list',
    urlPersonCreate: 'http://localhost:8080/SpringBoot-CRUD-Example/api/persons/create', 
    urlPersonUpdate: 'http://localhost:8080/SpringBoot-CRUD-Example/api/persons/update', 
    urlPersonDelete: 'http://localhost:8080/SpringBoot-CRUD-Example/api/persons/delete',  
    urlReadPersonEdit: 'http://localhost:8080/SpringBoot-CRUD-Example/api/persons/read/edit', 
    urlReadPersonDetail: 'http://localhost:8080/SpringBoot-CRUD-Example/api/persons/read/persondetails', 
    urlCheckEmailExist: 'http://localhost:8080/SpringBoot-CRUD-Example/api/persons/read/checkemailperson'
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
